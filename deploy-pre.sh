#!/bin/bash
CURRENT_FAVICON="src/img/favicon.ico"
PRE_FAVICON="src/img/favicon-pre.ico"
PRODUCTION_FAVICON="src/img/favicon-production.ico"
PRE_FAVICON_URL="https://pre.nota.space/favicon.ico?v=pre"
PRODUCTION_FAVICON_URL="https://nota.space/favicon.ico?v=production"
# Change the favicon and the link to it to pre
sed -i "" "s+$PRODUCTION_FAVICON_URL+$PRE_FAVICON_URL+" src/index.html
ln -f -v $PRE_FAVICON $CURRENT_FAVICON
npm run build-dev && rsync -au4 --delete dist/* pu@nota.space:/var/www/pre.nota.space/html/
# Change favicon and link back to to the production state
ln -f -v $PRODUCTION_FAVICON $CURRENT_FAVICON
sed -i "" "s+$PRE_FAVICON_URL+$PRODUCTION_FAVICON_URL+" src/index.html
