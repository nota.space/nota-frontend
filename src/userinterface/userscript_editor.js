import * as login from '../api/login.js';
import * as selection from '../selection.js';
import * as positions from '../positions.js';
import * as settings from '../settings.js';
import { getSketch, getHTMLOverlay, setHTMLOverlay } from '../main.js';
import ace from 'ace-builds';
import "ace-builds/webpack-resolver";


export const userscriptEditor = (calledViaShortcut) => {
  const display = 'block';
  const sketch = getSketch();
    let mousePoint = {
      x: sketch.mouseX,
      y: sketch.mouseY
    };
    let fragmentAtMouse = positions.getFragmentAt(mousePoint);
    if(fragmentAtMouse === undefined || fragmentAtMouse === null) {
      console.warn("No fragment at mouse. This might be an effect " +
      "of temporary scripts");
      return;
    }

    // fragmentAtMouse.userScriptModifierKeyReleased is used to check
    // whether the scripting shortcut was released
    if(calledViaShortcut) {
      fragmentAtMouse.userScriptModifierKeyReleased = false;
    }

    if(fragmentAtMouse.userscriptEditorOverlay !== null) {
      setHTMLOverlay(fragmentAtMouse.userscriptEditorOverlay);
      const elem = getHTMLOverlay().overlay.elt;
      elem.style.display = display;
      requestAnimationFrame(()=>{
        elem.focus();
        const ta = elem.querySelector('textarea');
        ta.focus();
      });
    }
    else {
      let zoomPage = document.getElementById('zoom-page');
      let p5elem = sketch.createDiv();
      p5elem.elt.style.display = display;
      let editorMenu = document.createElement('div');
      editorMenu.classList.add('editor-menu');
      editorMenu.style.width = 'calc(70vw - 4px)';
      editorMenu.style.top = 'calc(15vh)';
      editorMenu.style.left = '15vw';
      editorMenu.style.position = 'relative';
      editorMenu.style.background = 'lightcoral';
      editorMenu.style.borderTop = '2px solid #555';
      editorMenu.style.borderLeft = '2px solid #555';
      editorMenu.style.borderRight = '2px solid #555';
      editorMenu.style.borderBottom = '1px solid gray';
      editorMenu.style.fontFamily = 'monospace';
      editorMenu.textContent = 'Info! The script editor\'s undo is separate from nota\'s undo';
      let ta = sketch.createDiv().elt;
      ta.style.width = "70vw";
      ta.style.height = "70vh";
      ta.style.top = "15vh";
      ta.style.left = "15vw";
      ta.textContent="";
      if(fragmentAtMouse.scriptSourceString) {
        ta.innerHTML = fragmentAtMouse.scriptSourceString;
        if(!fragmentAtMouse.shouldBuildUserScript()) {
          ta.readOnly = true;
          ta.style.background = "#ccc";
          ta.style.color = "#666";
          ta.style.fontWeight = "bold";
        }
      }
      let editor = ace.edit(ta);
      editor.autoIndent = true;
      editor.setOptions({
        mode: "ace/mode/javascript",
        showPrintMargin: false
      });
      editor.setTheme("ace/theme/sqlserver")

      let elem = p5elem.elt;
      elem.classList.add('ace-parent');
      let downTarget;
      const setTarget = (event) => {
        event.preventDefault();
        downTarget = event.target;
      };
      elem.addEventListener('mousedown', setTarget);

      let keypressdefault = ta.onkeypress;
      ta.onkeyup = function(ev) {
        if(ev.key === settings.KEYS.SCRIPT) {
          fragmentAtMouse.userScriptModifierKeyReleased = true;
        }
      }
      ta.onkeypress = function(ev) {
        if(
          !fragmentAtMouse.userScriptModifierKeyReleased &&
          ev.key === settings.KEYS.SCRIPT
        ) {
          ev.preventDefault();
          return false;
        }
      };
      function endScripting() {
        getHTMLOverlay().overlay.elt.style.display = 'none';
        setHTMLOverlay(null);
        zoomPage.style.filter = "";
        let src = editor.getValue();
        function setUserScript(fragment) {
          // if the script has changed, the user should be
          // changed to the current user
          // do this before calling setTmpScript, as the
          // script will not build otherwise
          // TODO: the order should not matter, make sure
          // a script that should be executed, is built in
          // fragment, if it is not yet available
          if(src !== fragment.userscript.scriptSourceString) {
            let userCreds = login.getCreds();
            fragment.setScriptAuthorToCurrectUser();
          }
          fragment.setTmpScript(src, true);
        }
        if(selection.selectionContains(fragmentAtMouse)) {
          selection.getSelectedFragments().forEach(function(frag) {
            setUserScript(frag);
          });
        }
        else {
          setUserScript(fragmentAtMouse);
        }
      }
      elem.style.width = "100vw";
      elem.style.height = "100vh";
      elem.style.position = "absolute";
      elem.style.zIndex = "10";
      elem.style.top = "0";
      elem.style.left = "0";
      elem.style.background = "#fffa";
      elem.style.textAlign = "center";
      document.body.append(elem);
      zoomPage.style.filter = "blur(2px)";
      fragmentAtMouse.userscriptEditorOverlay = {overlay: p5elem};
      setHTMLOverlay(fragmentAtMouse.userscriptEditorOverlay);
      elem.addEventListener('mousemove', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('wheel', function(ev) {
        ev.stopPropagation();
      }, {passive:false});
      elem.addEventListener('keypress', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('keydown', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('dragstart', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('drag', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('dragend', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('dragenter', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('dragover', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('dragleave', function(ev) {
        ev.stopPropagation();
      });
      elem.addEventListener('drop', function(ev) {
        ev.stopPropagation();
      });
      ta.addEventListener('click', function(ev) {
        ev.stopPropagation();
      });
      ta.onkeydown = function(ev) {
        if(ev.code == 'Escape') {
          endScripting();
        }
      };
      elem.addEventListener('click', (ev) => {
        if(ev.target.id === downTarget.id) {
          endScripting();
        }
      });
      elem.append(editorMenu);
      elem.append(ta);
      requestAnimationFrame(()=>{
        elem.focus();
        ta.childNodes[0].focus();
      });
    }
}
