const path = require('path')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
let env = process.env.NODE_ENV

module.exports = {
  mode: env,
  devtool: 'inline-source-map',
  devtool: 'source-map',
  devServer: {
    static: ['dist'],
    proxy: {
      '/api': {
        target: {
          host: '0.0.0.0',
          protocol: 'http',
          port: 8000
        },
        pathRewrite: {
          '^/api': ''
        }
      },
      '/ws/refresh/': {
        target: 'ws://0.0.0.0:8000',
        ws: true
      },
    }
  },
  watchOptions: {
    aggregateTimeout: 1000
  },
  entry: [
    '@babel/polyfill',
    './src/index.js'
  ],
  output: {
    filename: 'main.[contenthash].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.(ttf)$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
          name: '[name].[ext]'
        }
      },
      {
        test: /\.(png|svg|jpg|gif|ico)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [[
              '@babel/preset-env',
              {
                useBuiltIns: 'entry',
                targets: '> 0.25%, not dead',
                corejs: '2'
              }
            ]],
            plugins: [
            ]
          }
        }
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      title: 'Caching',
      favicon: 'src/img/favicon.ico'
    })
  ]
}
